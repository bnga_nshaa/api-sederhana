const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const { ERROR: httpError, SUCCESS: http } = require('../../../helpers/http-status/status_code');

// Insert
const postContent = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, commandModel.content);
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.postContent(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err ? wrapper.response(res, 'fail', result, 'Insert Content', httpError.CONFLICT) : wrapper.response(res, 'success', result, 'Insert Content', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

// Update
const putContent = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, commandModel.contentUpdate);
  const putRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.putContent(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err ? wrapper.response(res, 'fail', result, 'Update Content', httpError.CONFLICT) : wrapper.response(res, 'success', result, 'Update Content', http.OK);
  };
  sendResponse(await putRequest(validatePayload));
};

// Get
const getContent = async (req, res) => {
  const { id } = req.params;
  const getData = async () => queryHandler.getContent(id);
  const sendResponse = async (result) => {
    result.err ? wrapper.response(res, 'fail', result, 'Get Content', httpError.NOT_FOUND) : wrapper.response(res, 'success', result, 'Get Content', http.OK);
  };
  sendResponse(await getData());
};

// Page
const getPageContent = async (req, res) => {
  const parameter = req.params;
  const getData = async () => queryHandler.getPageContent(parameter);
  const sendResponse = async (result) => {
    result.err ? wrapper.response(res, 'fail', result, 'Get Page Content', httpError.NOT_FOUND) : wrapper.response(res, 'success', result, 'Get Page Content', http.OK);
  };
  sendResponse(await getData());
};

// Search
const getSearchContent = async (req, res) => {
  const keyword = req.params;
  keyword.toString();
  const getData = async () => queryHandler.getSearchContent(keyword);
  const sendResponse = async (result) => {
    result.err ? wrapper.response(res, 'fail', result, 'Get Search Content', httpError.NOT_FOUND) : wrapper.response(res, 'success', result, 'Get Search Content', http.OK);
  };
  sendResponse(await getData());
};


// Delete
const deleteContent = async (req, res) => {
  const { id } = req.params;
  const deleteData = async () => commandHandler.deleteContent(id);
  const sendResponse = async (result) => {
    result.err ? wrapper.response(res, 'fail', result, 'Delete Content', httpError.INTERNAL_ERROR) : wrapper.response(res, 'success', result, 'Delete Content', http.OK);
  };
  sendResponse(await deleteData());
};

module.exports = {
  postContent,
  getContent,
  getPageContent,
  getSearchContent,
  putContent,
  deleteContent,
};
