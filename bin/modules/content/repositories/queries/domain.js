const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const logger = require('../../../../helpers/utils/logger');
const { InternalServerError, NotFoundError } = require('../../../../helpers/error');

class Content {
  constructor(db) {
    this.query = new Query(db);
  }

  async viewContent(id) {
    const content = await this.query.findContent({ id });
    if (content.err) {
      return wrapper.error(new NotFoundError('Can not find content'));
    }
    const { data } = content;
    return wrapper.data(data);
  }

  async viewPageContent(parameter) {
    const limit = 2;
    const { page } = parameter;
    const offset = limit * page;
    const pagination = {limit , offset};
    const content = await this.query.pageContent(pagination);
    if (content.err) {
      return wrapper.error(new NotFoundError('Can not find content'));
    }

    const { data } = content;
    return wrapper.data(data);
  }

  async searchContent(parameter) {
    const content = await this.query.searchContent(parameter);
    if (content.err) {
      return wrapper.error(new NotFoundError('Can not find content'));
    }
    const { data } = content;
    return wrapper.data(data);
  }
}

module.exports = Content;
