class Query {
  constructor(db) {
    this.db = db;
  }

  async findContent(parameter) {
    const { id } = parameter;
    const recordset = await this.db.prepareQuery('SELECT title, uploader, rating, total_user, details, material FROM content WHERE id = ?', [id]);
    return recordset;
  }

  async duplicateContent(parameter) {
    const { title, uploader, rating, totalUser, details, material } = parameter;
    const recordset = await this.db.prepareQuery(
      'SELECT title, uploader, rating, total_user, details, material FROM content WHERE title = ? AND uploader = ? AND rating = ? AND total_user = ? AND details = ? AND material = ?',
      [title, uploader, rating, totalUser, details, material]
    );
    return recordset;
  }

  async pageContent(parameter) {
    const { limit, offset } = parameter;
    const recordset = await this.db.prepareQuery('SELECT title, uploader, rating, total_user, details, material FROM content ORDER BY title ASC LIMIT ? OFFSET ?', [limit, offset]);
    return recordset;
  }

  async searchContent(parameter) {
    const { keyword } = parameter;
    const title = `%${keyword}%`;
    const uploader = `%${keyword}%`;
    const rating = `${parseInt(keyword)}%`;
    const total_user = `${parseInt(keyword)}%`;
    const details = `%${keyword}%`;
    const material = `%${keyword}%`;
    const recordset = await this.db.prepareQuery('SELECT title, uploader, rating, total_user, details, material FROM content WHERE title LIKE ? OR uploader LIKE ? OR rating LIKE ? OR total_user LIKE ? OR details LIKE ? OR material LIKE ?',
      [title, uploader, rating, total_user, details, material]
    );
    return recordset;
  }
}

module.exports = Query;
