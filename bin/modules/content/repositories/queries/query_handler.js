const Content = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');
const db = new Mysql(config.get('/mysqlConfig'));
const content = new Content(db);

const getContent = async (id) => {
  const getData = async () => {
    const result = await content.viewContent(id);
    return result;
  };
  const result = await getData();
  return result;
};

const getPageContent = async (parameter) => {
  const getPageData = async () => {
    const result = await content.viewPageContent(parameter);
    return result;
  };
  const result = await getPageData();
  return result;
};

const getSearchContent = async (parameter) => {
  const getSearchData = async () => {
    const result = await content.searchContent(parameter);
    return result;
  };
  const result = await getSearchData();
  return result;
};

module.exports = {
  getContent,
  getPageContent,
  getSearchContent
};
