const Contents = require('./domain');
const config = require('../../../../infra/configs/global_config');
const MySql = require('../../../../helpers/databases/mysql/db');
const dbSql = new MySql(config.get('/mysqlConfig'));

const content = new Contents(dbSql);

const postContent = async (payload) => {
  const postCommand = async (payload) => content.postContent(payload);
  return postCommand(payload);
};

const putContent = async (payload) => {
  const putCommand = async (payload) => content.putContent(payload);
  return putCommand(payload);
};
const deleteContent = async (id) => {
  const getData = async () => {
    const result = await content.deleteContent(id);
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  postContent,
  putContent,
  deleteContent,
};
